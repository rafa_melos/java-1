package br.com.neppo;

public class MathUtil {

    /**
     * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {
        int n;

        if(ints == null)
            throw  new IllegalArgumentException("Conjunto nao pode ser nulo!");
        n = 0;

        for(int x : ints)
        {
            n++;
        }

        return subsetSumRec(ints, n, sum);
    }

    public static boolean subsetSumRec(int ints[], int n, int sum)
    {
        boolean s;
        if(n == 0)
        {
            if(sum == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            s = subsetSumRec(ints, n-1, sum);
            if(s == false && ints[n-1] <= sum)
            {
                s = subsetSumRec(ints, n-1, sum-ints[n-1]);
            }
            return s;
        }
    }
}
